<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <meta name="description" content="website description goes here" />
  <meta name="keywords" content="keywords,goes,here" />
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

    <div id="container">
      <div id="header">
        <div id="logowrapper">
            <?php if ($logo) { ?>
            <div id="logo"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div><?php } ?>
            <?php if ($site_name) { ?><div id="siteName"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></div><?php } ?>
            <?php if ($site_slogan) { ?><div id="siteSlogan"><?php print $site_slogan ?></div><?php } ?>
        </div>
      <div id="nav">
      <?php if (isset($primary_links)) { ?><?php print theme('menu_links', $primary_links) ?> <?php } ?>
      </div>
    </div>
  
  <div id="content">
        <?php print $breadcrumb ?>
        <div class="pageTitle"><?php print $title ?></div>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
  </div>

  <div id="sidebar-wrapper">
    <div id="sidebar">
      <?php print $sidebar_right ?>
    </div>
  </div>


  <div id="footer">
    <?php print $footer_message ?>
  </div>
</div>
<?php print $closure ?>
</body>
</html>
