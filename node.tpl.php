  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><div class="nodeTitle"><a href="<?php print $node_url?>"><?php print $title?></a></div><?php }; ?>
    <span class="submitted"><?php print $submitted?></span>
    <br />
    <?php if ($terms) { ?><span class="taxonomy">Tags: <?php print $terms?></span><?php }; ?>
    <div class="content"><?php print $content?></div>
    <?php if ($links) { ?><div class="nodeLinks">&raquo; <?php print $links?></div><?php }; ?>
  </div>
