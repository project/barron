  <div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
    <?php if ($picture) {
    print $picture;
  } ?>
<h3 class="commentTitle"><?php print $title; ?></h3>
    <div class="submitted"><?php print $submitted; ?></div>
    <div class="content"><?php print $content; ?></div>
    <?php if ($links) { ?><div class="commentLinks"><?php print $links; ?></div><?php } ?>
  </div>
